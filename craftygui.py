import PySimpleGUI as sg
from crafty_client import CraftyWeb
import time
import sys
import os

#variables
APIKey = None
Host = None
cweb = None
error = None
SSL = False
theme = ''

#window
if not os.path.isfile("theme.txt"):
    sg.theme('DefaultNoMoreNagging')
    layout = [[sg.Text('Theme List')],
                [sg.Text('Please Enter The Name Of A Theme')],
                [sg.Listbox(values=sg.theme_list(),
                            size=(20, 12), key='-LIST-', enable_events=False)],
                [sg.InputText()],
                [sg.Submit()]]

    window = sg.Window('Choose A Theme!', layout)

    event, values = window.read()
    window.close()

    text_input = values[0]
    theme = text_input
    sg.theme(theme)
    file = open("theme.txt", "w")
    file.write(theme)
    file.close()
else:
    file = open("theme.txt", "r")
    Lines = file.readlines()
    theme = Lines[0]
    sg.theme(theme)



if os.path.isfile("settings.txt"):
    file = open("settings.txt", "r")
    Lines = file.readlines()
    Host = Lines[0]
    APIKey = Lines[1]
    SSL = Lines[2]
    cweb = CraftyWeb(Host, APIKey, bool(SSL))
else:
    Host = sg.PopupGetText("Please enter or re-enter your Host IP (put https:// in the beginning and :(port) at the end depending on what port you use (default is 443)", default_text="https://127.0.0.1:8000")
    APIKey = sg.PopupGetText("Please enter or re-enter your API Key")
    SSL = sg.PopupGetText("Do you want SSL Enabled or not (type anything for true or blank for false)")
    cweb = CraftyWeb(Host, APIKey, bool(SSL))
    file = open("settings.txt", "w")
    file.write(Host + "\n")
    file.write(APIKey + "\n")
    file.write(SSL + "\n")
    file.close()

layout = [
            [sg.Text('Crafty GUI'), sg.Text("Made By: Ben R")],
            [sg.Text("Start/Stop/Backup/Restart Servers")],
            [sg.Button("Start", key='_START_'), sg.Button("Stop", key='_STOP_'), sg.Button("Backup", key='_BACKUP_'), sg.Button("Restart", key='_RESTART_')],
            [sg.Text("Get Server Logs")],
            [sg.Button("Get Logs", key='_LOGS_')],
            [sg.Button("Run Command", key='_COMMAND_')],
            [sg.Button("Reconnect", key='_RECONNECT_')],
            [sg.Button("Refresh Servers", key='_REFRESH_')]
]


window = sg.Window('Crafty Client', layout)

def Start():
    try:
        global cweb
        ServerID = int(sg.PopupGetText("Enter Server ID: "))
        cweb.start_server(ServerID)
    except:
        e = sys.exc_info()[0]
        print(e)
        time.sleep(1)
def Stop():
    try:
        global cweb
        ServerID = int(sg.PopupGetText("Enter Server ID: "))
        cweb.stop_server(ServerID)
    except:
        e = sys.exc_info()[0]
        print(e)
        time.sleep(1)
def Backup():
    try:
        global cweb
        ServerID = int(sg.PopupGetText("Enter Server ID: "))
        cweb.backup_server(ServerID)
    except:
        e = sys.exc_info()[0]
        print(e)
        time.sleep(1)
def Restart():
    try:
        global cweb
        ServerID = int(sg.PopupGetText("Enter Server ID: "))
        cweb.restart_server(ServerID)
    except:
        e = sys.exc_info()[0]
        print(e)
        time.sleep(1)
def Logs():
    try:
        global cweb
        ServerID = int(sg.PopupGetText("Enter Server ID: "))
        try:
            StringData = ""
            data = cweb.get_server_logs(ServerID)
            for x in data:
                StringData = StringData + str(x) + "\n"
            sg.PopupScrolled(StringData,  title="Log", non_blocking=True)
        except ServerNotFound:
            if debug:
                print("Server Not Found Error Raised")
    except:
        e = sys.exc_info()[0]
        print(e)
        time.sleep(1)
def Command():
    try:
        global cweb
        ServerID = int(sg.PopupGetText("Enter Server ID: "))
        ExeCommand = sg.PopupGetText("Enter Command to Run: ")
        cweb.run_command(ServerID, ExeCommand)
    except:
        e = sys.exc_info()[0]
        print(e)
        time.sleep(1)

def Reconnect():
    global SSL
    global Host
    global APIKey
    global cweb
    SSL = False
    Host = None
    APIKey = None
    cweb = None

def Refresh():
    try:
        global cweb
        y = 0
        x = 0
        StringData = "ID, Name, Is Running, Is Auto Starting \n"
        data = cweb.list_mc_servers(AllEssentials=True)
        for item in data:
            if x >= 4:
                x = 0
                StringData = StringData + "\n"
            else:
                x += 1
                StringData = StringData + str(data(y)) + ', '
                y += 1
        sg.PopupScrolled(StringData, title="ServerList", non_blocking=True)
    except:
        e = sys.exc_info()[0]
        print(e)
        time.sleep(1)
FunctionDictionary = {
    '_START_'        : Start,
    '_STOP_'         : Stop,
    '_BACKUP_'       : Backup,
    '_RESTART_'      : Restart,
    '_LOGS_'         : Logs,
    '_COMMAND_'      : Command,
    '_RECONNECT_'    : Reconnect,
    '_REFRESH_'      : Refresh
}

#loop
while True:
    event, values = window.Read(timeout=100)
    if event is None or event == "Exit":
        break
    elif event in FunctionDictionary:
        FunctionToCall = FunctionDictionary[event]
        FunctionToCall()

window.close()
del window